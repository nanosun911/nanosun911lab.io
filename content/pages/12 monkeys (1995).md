---
categories: movie
date: 2021-11-07
title: "12 Monkeys (1995)"
---
updated:: 2021-11-08
alias:: [[Twelve Monkeys]] [[十二猴子]] 
rating:: 7
star:: [[Bruce Willis]] [[Madeleine Stowe]] [[Brad Pitt]]
[url](https://www.imdb.com/title/tt0114746/)
[url-douban](https://m.douban.com/movie/subject/1298744/)
这个电影就挺有年代感的，或者说有风格，有点cult, 有点[[大卫林奇]]的感觉。。布拉德皮特确实很出彩。女主有点太漂亮。