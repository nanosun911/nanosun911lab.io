---
categories: movie
date: 2010-12-11
title: "3 Idiots (2009)"
---
updated:: 2021-09-02
alias:: 三傻大闹宝莱坞
director:: [[Rajkumar Hirani]]
rating:: 10
star:: [[Aamir Khan]]
[url](https://www.imdb.com/title/tt1187043/)
[url-douban](https://movie.douban.com/subject/3793023/)
