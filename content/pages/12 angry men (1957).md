---
categories: movie
date: 2009-06-22
title: "12 Angry Men (1957)"
---
updated:: 2021-09-02
alias:: 十二怒汉
director:: [[Sidney Lumet]]
rating:: 10
[url](https://www.imdb.com/title/tt0050083/)
