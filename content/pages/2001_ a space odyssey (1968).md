---
categories: movie
date: 2015-01-09
title: "2001: A Space Odyssey (1968)"
---
updated:: 2021-03-19
director:: [[Stanley Kubrick]]
rating:: 9
[url](https://www.imdb.com/title/tt0062622/)
