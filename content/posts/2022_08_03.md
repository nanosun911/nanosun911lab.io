---
title: August 3, 2022 Nan
date: 2022-08-03
---
- setup [[HUGO]] to generate fulltex rss then use [[IFTTT]] to import to blogger. I can do this all within code-server.
	- used a theme called [smol](https://github.com/colorchestra/smol)
	- theme is cloned into hugo folder, then to compile it, need to add it to the submodule list, which I don't know how to do. So I just removed `.git` folder.
- 沁沁今天陪我跑步！走了一条相对比较平的路线，又不晒，感觉还不错，距离可以在2-5km 之间自己调整。今天跑了1/3, 走了剩下的部分，也挺累的。
	- woodland - clubhouse
	- [strava](https://strava.app.link/TSOifjKNcsb)
- 晚饭吃的西红柿鸡蛋面+羊腿肉，这次羊腿肉20块吃了四天，还是很值的