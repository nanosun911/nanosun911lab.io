---
title: August 21, 2022 Nan
date: 2022-08-22
---
- 早上撒肥料（沁沁撒的），三袋 [Milorganite](<https://notes.nanoyun.net/#/page/Milorganite>). String trim, hedge trim, 扫地，吹风，割草，撒了点种子. #lawncare  
	- 每年要撒 Nitrogen 3lb/1000sqft, so in total 45 lbs. This year so far applied 18 lbs / 45 lbs.  
- 午饭继续吃 [红烧牛尾](<https://notes.nanoyun.net/#/page/红烧牛尾>), 还配了朝鲜冷面，凉拌黄瓜。  
- 下午去逛了 Tanger outlet，买了七八件衣服，12双袜子。我现在最喜欢的店是Columbia, 能买到我想买的衣服。  
- 晚饭川菜！Lancaster 城西的 巴蜀人家 Szechuan Gourmet。 挺不错的，上菜很快，关键是他们有川北凉粉！  
- 晚上看了一集  [House of the Dragon](<https://notes.nanoyun.net/#/page/House of the Dragon>), 还不错，目前看起来是宫斗剧的节奏。  
- 还看了一集 National Geographic 的 [[Wild Yellowstone (2020)]]，美式野生动物纪录片，很有特色，明显和BBC的不一样，我觉得可以总结为更多切镜头和更多慢镜头，总体让人感觉节奏还挺快的。  