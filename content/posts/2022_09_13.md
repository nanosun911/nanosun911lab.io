---
title: September 13, 2022 Nan
date: 2022-09-14
---
- Qin needs to schedule visa appointment but it's not easy. trying [tuixue.online](https://tuixue.online/visa/)  
	- Qin is also exploring other option such as H4.  
- [Better Call Saul](<https://notes.nanoyun.net/#/page/Better Call Saul>) 艾美奖颗粒无收，败给了 [Succession](<https://notes.nanoyun.net/#/page/Succession>) 和 [[Euhporia]]  
- Received [[Steam Deck]]. Set it up. All working great. Smells like my first Motorola! Installed steam game, Heroic launcher and Emudeck. many emulator games can play well. only need to add good cover art.  
- 早上开车听了些李天豪  
	- 首先讲戈尔巴乔夫，听他的版本貌似戈尔巴乔夫真的很激进，亲手恨不得加速把苏联给拆了，也算另一种视角  
	- 再讲左宗棠收复新疆的部分，听到感觉还是很激动，虽然明白这很民族主义。听左宗棠开疆拓土、保家卫国，就是比听外国人做类似的事情要更带感。另外左宗棠这一段历史还有一个唏嘘的点就是当时收新疆还是发展海军防日本，避免后来甲午战争的惨败，引人遐想。  
- 晚上开车主要听  
	- [Stoner](<https://notes.nanoyun.net/#/page/Stoner>) 太生气了。气到我内伤。不过看这书我总能体会到一种我理解的入定的感觉，就是我既跟着叙述带入主人公，体验记录着自己的经历，又可以经常跳出来，从第三者的角度审视这个主人公是不是真诚的，是不是在叙述中有美化，有隐瞒，是不是一些主人公讨厌的人并没有那么坏，主人公喜欢的人也并没有那么美。我觉得也许作者是有意在创造这种感觉，因为在书里他就提到了主人公自己也会有这种感觉。  
	- [卡拉马佐夫兄弟](<https://notes.nanoyun.net/#/page/卡拉马佐夫兄弟>)，听这看听着像也挺快的，已经完成6%了。确实是神作，已经快把我弄哭了。高晓松说的挺好，能量密度很大。目前这一段让我有个感想就是，好莱坞电影经常弄一个大冲突，最后一个爱字就都迎刃而解，合家欢，比如最近的 [[The Matrix (1999)]] 和续作. 但也许这并不是烂俗，而就是真理。 