---
title: September 15, 2022 Nan
date: 2022-09-16
---
- Qin's visa problem  
	-  
	  > The outcome of a Labor Certification filing is not connected to the time the beneficiary has remaining in H-1B status. A Labor Certification application decision does not depend upon the immigration status of the alien beneficiary. The U.S. Green Card application is related to a future job offer concept, unlike the ability to obtain H-1B extensions to live and work legally in U.S.  
	  [ref](https://www.murthy.com/2017/05/08/filing-labor-certification-during-sixth-year-of-h1b/)  

	- Based on this quote, PERM application can still carry on after transfer to H4. and Qin can definitely continue with H1B on 2023-1-30, that is 365 days after PERM application.  
- 昨天晚上睡 Bald eagle营地，睡车里，补上了上次欠的22块，但这次的钱也先欠着。确实算是野外，晚上特别安静，特别黑。住营地贵但是可以睡的安心，睡车里还是更好，车外晚上非常潮湿。
- 开车听了点机核的广播聊特殊宠物，才知道蛇寿命还很长，可以到五十年。我就想到 [raised by wolves](<https://notes.nanoyun.net/#/page/raised by wolves>)的导演或者编剧可能是有养蛇的。才能抓到那么多蛇会引起人原始恐惧的点, 而恐惧的反面就是崇拜。  
- bought saturday tickets of Hershey RV show 
- 今天是个很重要的日子啊，以太坊 Etherium 发生了啥，从今天开始，显卡挖矿就没有效率可言了。持续了两三年的挖矿热和显卡溢价应该都要结束了。  