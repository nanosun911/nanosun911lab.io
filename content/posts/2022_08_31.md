---
title: August 31, 2022 Nan
date: 2022-09-01
---
- Morning drive  
	- Saw an [article (w)](https://wallabag.nanoyun.net/share/63111b85d895c2.13480535) on [New Yorker](https://www.newyorker.com/magazine/2019/03/18/john-williams-and-the-canon-that-might-have-been?mbid=social_twitter&s=09) about John Williams, and [Stoner](<https://notes.nanoyun.net/#/page/Stoner>). But the article was too hard to follow. And just realized that the writer and the composer are both John Williams.  
	- An [article](https://www.reuters.com/world/obituary-gorbachev-ended-cold-war-presided-over-soviet-collapse-2022-08-30/?s=09) about Gorbachev, who died yesterday. It is a obituary, probably written long time ago well before He did die. 总的来说戈尔巴乔夫是一个做了改革的人，是希望改善人民生活，也确实帮助统一了德国，终结了冷战，但是在政治斗争中失败了，还好最后可以善终。我忽然想到另一个人有点相似，就是赵紫阳。
	- About camping outdoor portable fridge, seems a cooler is good enough for us because it can stay cool for 2 days.  
	- 听 大聪看电影 说了 [House of the Dragon](<https://notes.nanoyun.net/#/page/House of the Dragon>) 第二集，还算了解到一些新信息，首先片头很多血流啊流的貌似是家谱，literally 血脉传承。另外解释了奥托主动去龙石岛可能是故意要去挑事的，根本就没想解决问题，我还是太嫩了，当时没看出这一点。还讲解了剧情跟原著的关系，有改编也有补充，还是那句话，原著很简略很多留白，改编空间很大。大聪觉得到目前剧情都编的不错，还补充了国王不娶12岁外甥女是有算计的，是想要平衡奥托家和白头发海军大臣家两股势力。但到目前为止我是有些怀疑的，我觉得第二集最后国王选择奥托的女儿结婚是铺垫不够的，有点为反转而反转。另外目前对戴蒙的刻画也有点奇怪，到底是傻逼还是有勇有谋呢，不懂。
- Tested wood gas stove, works well! boiled a pot of water for dinner 螺蛳粉!  
- 晚上我也睡了会儿充气床，还挺舒服的，而且好像还可以把气打得更足一点。  
