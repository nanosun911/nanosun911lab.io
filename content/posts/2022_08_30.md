---
title: August 30, 2022 Nan
date: 2022-08-31
---
- Morning drive  
	- AMD 7000 series is released  
	- TODO #podcast about [[活出生命的意义]]，#文化有限  
	- listened to a piece, [我如何每日从北京酒仙桥到加州硅谷上班](<https://notes.nanoyun.net/#/page/我如何每日从北京酒仙桥到加州硅谷上班>) ， 一开头让人觉得文笔很一般，但是因为它有一个很好的IDEA, 可以掩饰文笔的不足. 听这篇文章是因为大聪说电影讲开发者的时候提到了这个。之后又想听之前加入书单很久的 [自踢几踵](https://wallabag.nanoyun.net/share/630fd2ec6e6f38.91176372)，但是有点难，这书里太多人互相讲话，听的时候就完全搞不清楚是谁在讲话。  
- I can totally use my [[wallabag]] service as a internet archive. It saves a local copy of the article, mitigates the risk of the site going down or article removed. It also support highlighting, annotation and tagging. Also output a public link.  
- Bought some camping gear  
	- Want to write a longer article about things that are different in US camping vs Asian camping [article](<https://notes.nanoyun.net/#/page/Things you don't need when camping in the US>)  
	- bought a air mattress(walmart), another hammock, a tent pole and a blanket (potentially also can be used as a hammock underquilt). wanted to get an adapter for 12V air mattress pump, but I can test my portable one first.  
	- the air mattress can be inflated with the small portable electric pump, but takes very long, about 20 minutes. And the pump turned very hot. the mattress itself feels good. Although it's only $18 and 10 inches thick while there are other product costs $60 and 15 inches.  
