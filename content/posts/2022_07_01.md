---
title: July 1, 2022 Nan
date: 2022-07-01
---
- 下载了录音转写软件 Otter  开始试用，每月有600分钟免费，应该足够了  
- 出门玩，大约九点半出发，本来应该四点到，但一路太堵了，四点半才开到new haven  
- 午饭吃 Bella burger shack, 很家庭风的小店，味道不错  
- 路上停麦当劳吃奶昔，太甜，纸吸管  
- 路上听了点 [Atomic Habits](<https://notes.nanoyun.net/#/page/Atomic Habits>)  
- 逛Yale, 比预期的好太多，耶鲁的建筑是很高大上的，也有挺多风格，花园像是挺新的，搞得很好  
- 去了new Haven 的little Italy 吃 apizza , 有两家店排队很长，我们就选了个不排队的zenlini, 味道也很不错！还喝了意大利罗马的啤酒。披萨的好应该就体现在面饼的筋道上吧  
- 最后去海边的lighthouse point park, 有灯塔，有旋转木马，还有人办婚礼在唱歌跳舞  
- 晚上住west Greenwich 附近的best western , 离 明天的目标 Newport 还有一小时  
- 晚上看了一集 [Stranger things](<https://notes.nanoyun.net/#/page/Stranger things>)  